# Open source & Creative Commons attributions

1. Background image is [**1800s Library**](https://www.flickr.com/photos/98640399@N08/10030588973/in/photolist-ghnoDc-f5Z7YW) (with modifications) by **Barta IV** on [Flickr](https://www.flickr.com/photos/98640399@N08/).

    License is Creative Commons [Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/)
