# biblioteka

A home library management system in Django, developed in free time and still has a long way to go.

You can see a demo of the master branch [here](https://dev.marcinzyla.pl).

# Installation (If everything goes ok, which it probably won't)

Warning: this is only for testing/messing around with and __not suitable for production__.

You will need `git`, `python3.4` (or higher), `pip` (for python 3) and `virtualenv` installed. Use your distribution's package manager to obtain them.

1. `$ git clone https://gitlab.com/rzyua/biblioteka.git`
2. `$ cd biblioteka`
3. `$ virtualenv . --no-site-packages`
4. `$ source bin/activate`
5. `$ pip3 install -r requirements.txt`
6. `$ python3 manage.py createsuperuser`
7. `$ ./migrateandrun.sh`
8. Go to `http://localhost:8000` and enjoy

# Picture, because everyone loves pictures

![](screenshots/1.png)
