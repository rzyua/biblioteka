from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from .models import Author, Tag, Publisher, Book, Rental, UserProfile


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name = _('User profile')
    verbose_name_plural = _('User profile')


class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)


# Register your models here.
admin.site.register(Author)
admin.site.register(Tag)
admin.site.register(Book)
admin.site.register(Publisher)
admin.site.register(Rental)