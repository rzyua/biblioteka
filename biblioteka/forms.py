from django import forms
from django.db import models
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Reset, Div, HTML
from crispy_forms.bootstrap import StrictButton

from .models import Book, Author, Tag, Publisher


class BookModelForm(forms.ModelForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super(BookModelForm, self).__init__(*args, **kwargs)

        self.helper.html5_required = True

        self.helper.layout = Layout(
            'title',
            Div(
                Div(
                    'ISBN',
                    css_class='col-md-6'
                ),
                Div(
                    'copies',
                    css_class='col-md-6'
                ),
                css_class='row'
            ),
            Div(
                Div(
                    'authors',
                    'publisher',
                    css_class='col-md-6'
                ),
                Div(
                    'tags',
                    'pub_year',
                    css_class='col-md-6'
                ),
                css_class='row'
            ),
            'comment',
            StrictButton(
                '<span class="glyphicon glyphicon-save"></span> ' + _('Save'),
                css_class='btn-primary',
                type='Submit',
                value='Save'
            ),
            StrictButton(
                '<span class="glyphicon glyphicon-repeat"></span> ' + _('Reset'),
                css_class='btn-warning',
                type='Reset',
            ),
        )

    class Meta:
        model = Book
        fields = ['ISBN', 'copies', 'title', 'authors', 'tags', 'publisher', 'pub_year', 'comment']
        widgets = {
            'comment': forms.Textarea(attrs={'rows': 4}),
        }


class AuthorModelForm(forms.ModelForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super(AuthorModelForm, self).__init__(*args, **kwargs)

        self.helper.html5_required = True

        self.helper.layout = Layout(
            Div(
                Div(
                    'first_name',
                    css_class='col-md-6'
                ),
                Div(
                    'last_name',
                    css_class='col-md-6'
                ),
                css_class='row'
            ),
            'comment',
            StrictButton('<span class="glyphicon glyphicon-save"></span> ' + _('Save'),
                css_class='btn-primary',
                type='Submit',
                value='Save'
            ),
            StrictButton(
                '<span class="glyphicon glyphicon-repeat"></span> ' + _('Reset'),
                css_class='btn-warning',
                type='Reset',
            ),
        )

    class Meta:
        model = Author
        fields = ['first_name', 'last_name']


class TagModelForm(forms.ModelForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super(TagModelForm, self).__init__(*args, **kwargs)

        self.helper.html5_required = True

        self.helper.layout = Layout(
            'name',
            StrictButton(
                '<span class="glyphicon glyphicon-save"></span> ' +_('Save'),
                css_class='btn-primary',
                type='Submit',
                value='Save'
            ),
            StrictButton(
                '<span class="glyphicon glyphicon-repeat"></span> ' + _('Reset'),
                css_class='btn-warning',
                type='Reset',
            ),
        )

    class Meta:
        model = Tag
        fields = ['name']


class PublisherModelForm(forms.ModelForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super(PublisherModelForm, self).__init__(*args, **kwargs)

        self.helper.html5_required = True

        self.helper.layout = Layout(
            'name',
            StrictButton(
                '<span class="glyphicon glyphicon-save"></span> ' +_('Save'),
                css_class='btn-primary',
                type='Submit',
                value='Save'
            ),
            StrictButton(
                '<span class="glyphicon glyphicon-repeat"></span> ' + _('Reset'),
                css_class='btn-warning',
                type='Reset',
            ),
        )

    class Meta:
        model = Publisher
        fields = ['name']


class LoginForm(AuthenticationForm):
    helper = FormHelper()

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                _("This account is inactive."),
                code='inactive',
            )

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.helper.html5_required = True
        self.helper.form_class='form-signin'
        self.helper.label_class='sr-only'

        self.fields['username'].widget.attrs={'placeholder': _('Username')}
        self.fields['password'].widget.attrs={'placeholder': _('Password')}

        self.helper.layout = Layout(
            HTML('<h2 class="form-signin-heading">' + _('Sign in') + '</h2>'),
            'username',
            'password',
            StrictButton(
                '<span class="glyphicon glyphicon-log-in"></span> ' + _('Sign in'),
                css_class='btn-primary btn-block btn-lg',
                type='Submit',
                value='Save'
            ),
        )


class TestForm(forms.Form):
    isbn = forms.RegexField(
        regex="^(978|979)([0-9]){10}$",
        label='ISBN',
        required=False,
        min_length=13,
        max_length=13,
    )

    title = forms.CharField(
        label='Tytuł',
        required=True,
        max_length=512,
    )

    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super(TestForm, self).__init__(*args, **kwargs)
        self.helper.form_method = 'POST'
        self.helper.form_action = 'biblioteka:test'
        self.helper.html5_required = True

        self.helper.layout = Layout(
            Div(
                Div(
                    'isbn',
                    css_class='col-md-6',
                ),
                Div(
                    'title',
                    css_class='col-md-6',
                ),
                css_class='row',
            ),
            Div(
                StrictButton(
                    '<span class="glyphicon glyphicon-save"></span> Zapisz',
                    css_class='btn-warning',
                    type='Submit',
                ),
                Reset('Reset', 'Resetuj', css_class='btn-warning'),
                css_class='form-group'
            )
        )
