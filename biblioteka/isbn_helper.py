from .models import Book, Author, Publisher
from isbntools.app import meta


def book_from_isbn(isbn, service='merge'):

    try:
        book = Book.objects.get(ISBN=isbn)
    except:
        book = Book()
        book.ISBN = isbn

    authors = []
    publisher = None
    pub_year = None

    book_data = meta(isbn, service)

    if book_data:
        if book_data['Title']:
            if not book.title:
                book.title = book_data['Title']
        else:
            if not book.title:
                book.title = isbn

        if book_data['Authors']:
            for author in book_data['Authors']:
                if author:
                    # Attempt to check if the author exists in the db (last word is treated as last name)
                    if len(author.split()) > 1:
                        a = Author.objects.filter(first_name=author.rsplit(' ', 1)[0], last_name=author.split()[-1])
                    else:
                        a = Author.objects.filter(first_name='', last_name=author)

                    if a:
                        a = a[0]
                    else:
                        a = Author()

                        if len(author.split()) > 1:
                            a.first_name = author.rsplit(' ', 1)[0]
                            a.last_name = author.split()[-1]
                        else:
                            a.last_name = author

                    authors.append(a)

        if book_data['Publisher']:
            p = Publisher.objects.get(name=book_data['Publisher'])
            if p:
                publisher = p
            else:
                publisher = Publisher()
                publisher.name = book_data['Publisher']

        if book_data['Year']:
            pub_year = book_data['Year']

    return {'book': book,
            'authors': authors,
            'publisher': publisher,
            'pub_year': pub_year}
