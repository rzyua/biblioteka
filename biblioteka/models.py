from datetime import datetime

from django.core import validators
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Author(models.Model):
    first_name = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        verbose_name=_('first name'),
    )
    last_name = models.CharField(
        max_length=150,
        verbose_name=_('last name'),
    )

    def __str__(self):
        name = self.last_name

        if self.first_name:
            name = self.first_name + " " + self.last_name
        return name

    def books(self):
        return Book.objects.filter(authors=self.id)

    def get_absolute_url(self):
        return reverse('biblioteka:authorDetail', kwargs={'pk': self.pk})

    def _get_full_name(self):
        if self.first_name:
            return self.first_name + " " + self.last_name

        return self.last_name

    full_name = property(_get_full_name)


class Publisher(models.Model):
    name = models.CharField(
        max_length=300,
        verbose_name=_('name'),
    )

    def __str__(self):
        return self.name;

    def books(self):
        return Book.objects.filter(publisher=self.id)


class Tag(models.Model):
    name = models.CharField(
        max_length=32,
        unique=True,
        verbose_name=_('name'),
    )

    def __str__(self):
        return self.name

    def books(self):
        return Book.objects.filter(tags=self.id)

    def popularity(self):
        return Book.objects.filter(tags=self.id).count()

    def get_absolute_url(self):
        return reverse('biblioteka:tagDetail', kwargs={'pk': self.pk})


class Book(models.Model):
    isbn = models.CharField(max_length=13,
                            null=True,
                            blank=True,
                            unique=True,
                            validators=[validators.RegexValidator(regex="^(978|979)([0-9]){10}$",
                                                                  message=_('This is not a valid ISBN.'))],
                            name='ISBN',
                            verbose_name=_('ISBN')
                            )

    title = models.CharField(
        max_length=500,
        verbose_name=_('title')
    )

    copies = models.PositiveSmallIntegerField(
        validators=[validators.MinValueValidator(1)],
        default=1,
        verbose_name=_('no of copies'),
    )

    authors = models.ManyToManyField(
        Author,
        blank=True,
        verbose_name=_('authors'),
    )

    tags = models.ManyToManyField(
        Tag,
        blank=True,
        verbose_name=_('tags'),
    )

    publisher = models.ForeignKey(
        Publisher,
        blank=True,
        null=True,
        verbose_name=_('publisher'),
        on_delete=models.SET_NULL,
    )

    pub_year = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('year'),
    )

    comment = models.CharField(
        max_length=500,
        null=True,
        blank=True,
        verbose_name=_('comment')
    )

    def available_copies(self):
        return self.copies - self.rentals().filter(returned_date=None).count()

    def rentals(self):
        return Rental.objects.filter(book=self.id)

    def save(self):
        """overriding save method so that we can save Null to database, instead of empty string (project requirement)"""
        # get a list of all model fields (i.e. self._meta.fields)...
        emptystringfields = [field for field in self._meta.fields \
                             # ...that are of type CharField or Textfield...
                             if ((type(field) == models.fields.CharField) or (type(field) == models.fields.TextField)) \
                             # ...and that contain the empty string
                             and (getattr(self, field.name) == "")]
        # set each of these fields to None (which tells Django to save Null)
        for field in emptystringfields:
            setattr(self, field.name, None)
            # call the super.save() method
        super(Book, self).save()

    class Meta:
        permissions = (
            ("mark_book_read", _('Can mark a book as read.')),
            ("mark_book_unread", _('Can mark a book as unread.')),
        )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('biblioteka:bookDetail', kwargs={'pk': self.pk})


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    nickname = models.CharField(
        verbose_name=_('nickname'),
        null=True,
        blank=True,
        max_length=32,
    )

    books_read = models.ManyToManyField(
        Book,
        blank=True,
        verbose_name=_('read books'),
    )

    def mark_book_read(self, book_id):
        book = Book.objects.get(id=book_id)

        if not book:
            raise ValidationError(_("Book doesn't exist."))

        self.books_read.add(book)

    def mark_book_unread(self, book_id):
        book = Book.objects.get(id=book_id)

        if not book:
            raise ValidationError(_("Book doesn't exist."))

        self.books_read.remove(book)

    def rentals(self):
        return Rental.objects.filter(rented_by=self.user)


class Rental(models.Model):
    book = models.ForeignKey(
        Book,
        on_delete=models.CASCADE,
        verbose_name=_('rented book'),
    )

    rented_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('rented by'),
    )

    rented_date = models.DateTimeField(
        verbose_name=_('rented date'),
    )

    returned_date = models.DateTimeField(
        null=True,
        blank=True,
    )

    class Meta:
        permissions = (
            ("rent_book", _('Can rent a book for themselves')),
            ("return_book", _('Can return a rented book')),
            ("rent_book_for_user", _('Can create a rental for someone else')),
            ("return_book_for_user", _('Can return a book that was borrowed by someone else')),
        )

    def __str__(self):
        return self.rented_by.__str__() + ": " + self.book.title

    def clean(self):
        if not self.pk and self.book.available_copies() < 1:
            raise ValidationError(_('No copies available.'), code='out_of_copies')

    def return_book(self, user):
        if self.rented_by != user:
            raise ValidationError(_('You can\'t return a book that was rented by someone else.'), code='wrong_user')
        self.returned_date = datetime.now()
        self.save()
