from django.db.models.signals import post_save, Signal
from django.contrib.auth.models import User
from django.dispatch import receiver

from biblioteka.models import UserProfile


@receiver(post_save)
def create_user_profile(sender, **kwargs):
    if sender == User and kwargs['created'] == True:
        profile = UserProfile()
        profile.user = kwargs['instance']
        profile.clean()
        profile.save()

