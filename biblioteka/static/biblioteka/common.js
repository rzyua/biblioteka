$(document).ready(function()
    {
        $("#book_table").tablesorter({cssAsc: "headerSortUp",
                                       cssDesc: "headerSortDown",
                                       cssHeader: "headerUnsorted",
                                       sortList: [[0,0]]});

        $("#author_table").tablesorter({cssAsc: "headerSortUp",
                                       cssDesc: "headerSortDown",
                                       cssHeader: "headerUnsorted",
                                       sortList: [[1,0]]});

        $("#tag_table").tablesorter({cssAsc: "headerSortUp",
                                       cssDesc: "headerSortDown",
                                       cssHeader: "headerUnsorted",
                                       sortList: [[0,0]]});

        $("#publisher_table").tablesorter({cssAsc: "headerSortUp",
                                       cssDesc: "headerSortDown",
                                       cssHeader: "headerUnsorted",
                                       sortList: [[0,0]]});
    }
);