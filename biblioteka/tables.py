import django_tables2 as tables
from django.utils.translation import ugettext as _
from .models import Tag


class TagTable(tables.Table):

    class Meta:
        model = Tag
        exclude = ['id']
        empty_text= _('Empty.')

    def __init__(self, *args, **kwargs):
        super(TagTable, self).__init__(*args, **kwargs)

        self.template = 'django_tables2/bootstrap.html'