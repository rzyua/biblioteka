from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .forms import LoginForm
from . import views

app_name = 'biblioteka'

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^user/login/&', auth_views.login, {'authentication_form': LoginForm}, name='login'),
    url(r'^user/logout/', auth_views.logout, {'next_page': 'biblioteka:index'}, name='logout'),

    url(r'^user/profile/$', views.user_profile, name='userProfile'),

    url(r'^test/$', views.test, name='test'),

    url(r'^book/(?P<pk>[0-9]+)/$',        views.BookDetailView.as_view(), name='bookDetail'),
    url(r'^book/create$',                 views.book_create_view,         name='bookCreate'),
    url(r'^book/(?P<pk>[0-9]+)/update/$', views.book_update_view,         name='bookUpdate'),
    url(r'^book/(?P<pk>[0-9]+)/delete/$', views.BookDeleteView.as_view(), name='bookDelete'),
    url(r'^book/$',                       views.BookListView.as_view(),   name='bookList'),

    url(r'^book/create/isbn/$', views.isbn, name='ISBN'),

    url(r'^book/(?P<book_id>[0-9]+)/mark_read/$',   views.mark_book_read_view,   name='bookMarkRead'),
    url(r'^book/(?P<book_id>[0-9]+)/mark_unread/$', views.mark_book_unread_view, name='bookMarkUnread'),

    url(r'^rental/(?P<book_id>[0-9]+)/create/$',   views.rental_create_view, name='rentalCreate'),
    url(r'^rental/(?P<rental_id>[0-9]+)/return/$', views.rental_return_view, name='rentalReturn'),

    url(r'^author/(?P<pk>[0-9]+)/$',        views.AuthorDetailView.as_view(), name='authorDetail'),
    url(r'^author/$',                       views.AuthorListView.as_view(),   name='authorList'),
    url(r'^author/create$',                 views.author_create_view,         name='authorCreate'),
    url(r'^author/(?P<pk>[0-9]+)/update/$', views.author_update_view,         name='authorUpdate'),
    url(r'^author/(?P<pk>[0-9]+)/delete/$', views.AuthorDeleteView.as_view(), name='authorDelete'),

    url(r'^tag/(?P<pk>[0-9]+)/$',        views.TagDetailView.as_view(), name='tagDetail'),
    url(r'^tag/$',                       views.TagListView.as_view(),   name='tagList'),
    url(r'^tag/create$',                 views.tag_create_view,         name='tagCreate'),
    url(r'^tag/(?P<pk>[0-9]+)/update/$', views.tag_update_view,         name='tagUpdate'),
    url(r'^tag/(?P<pk>[0-9]+)/delete/$', views.TagDeleteView.as_view(), name='tagDelete'),

    url(r'^publisher/(?P<pk>[0-9]+)/$',        views.PublisherDetailView.as_view(), name='publisherDetail'),
    url(r'^publisher/$',                       views.PublisherListView.as_view(),   name='publisherList'),
    url(r'^publisher/create$',                 views.publisher_create_view,         name='publisherCreate'),
    url(r'^publisher/(?P<pk>[0-9]+)/update/$', views.publisher_update_view,         name='publisherUpdate'),
    url(r'^publisher/(?P<pk>[0-9]+)/delete/$', views.PublisherDeleteView.as_view(), name='publisherDelete'),
]
