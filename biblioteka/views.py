from datetime import datetime

from django.http import HttpResponseRedirect, Http404
from django.forms.models import model_to_dict
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ValidationError
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.views import generic

from django_tables2 import RequestConfig
from isbnlib._exceptions import NotValidISBNError

from .forms import BookModelForm, AuthorModelForm, TagModelForm, PublisherModelForm, TestForm
from .tables import TagTable
from .models import Book, Author, Tag, Publisher, Rental
from .isbn_helper import book_from_isbn


def index(request):
    return redirect(reverse('biblioteka:bookList'))


@login_required
def user_profile(request):
    return render(request, 'registration/user_profile.html')


@login_required
def test(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TestForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required

                # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = TestForm()
    return render(request, 'biblioteka/test.html', context={'form': form})


@login_required
def isbn(request):
    if request.method == 'POST':
        try:
            data = book_from_isbn(request.POST.get('ISBN'))
        except NotValidISBNError as e:
            messages.add_message(request, messages.ERROR, _('This ISBN is incorrect.'))
            return redirect('biblioteka:bookList')

        book = data['book']

        if not book.pk:
            messages.add_message(request, messages.SUCCESS, _('Book added.'))
            book.save()
        else:
            messages.add_message(request, messages.WARNING, _('Book updated.'))

        authors = data['authors']

        for author in authors:
            if not author.pk:
                author.save()

            book.authors.add(author)

        publisher = data['publisher']

        if publisher and not book.publisher:
            if not publisher.id:
                publisher.save()

            book.publisher = publisher

        pub_year = data['pub_year']
        if pub_year and not book.pub_year:
            book.pub_year = pub_year

        book.save()

        return redirect(reverse('biblioteka:bookDetail', args=[book.pk]))
    else:
        raise Http404()


@login_required
@permission_required('biblioteka.add_book')
def book_create_view(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = BookModelForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            saved = form.save()
            # add a success message
            messages.add_message(request, messages.SUCCESS, _('Book added.'))
            # redirect to a new URL:
            return redirect(reverse_lazy('biblioteka:bookDetail', kwargs={'pk': saved.pk}))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = BookModelForm()
    return render(request, 'biblioteka/book_create_form.html', context={'form': form})


@login_required
@permission_required('biblioteka.change_book')
def book_update_view(request, pk):
    instance = get_object_or_404(Book, pk=pk)

    if request.method == 'POST':
        form = BookModelForm(request.POST, instance=instance)
        if form.is_valid():
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Book updated.'))
            return redirect(reverse_lazy('biblioteka:bookDetail', kwargs={'pk': saved.pk}))

    else:
        form = BookModelForm(initial=model_to_dict(instance), auto_id=False)
    return render(request, 'biblioteka/book_update_form.html', context={'form': form})


class BookListView(generic.ListView):
    model = Book


class BookDetailView(generic.DetailView):
    model = Book


class BookDeleteView(PermissionRequiredMixin, LoginRequiredMixin, SuccessMessageMixin, generic.DeleteView):
    model = Book
    success_url = reverse_lazy('biblioteka:bookList')
    success_message = _('Book deleted.')
    permission_required = 'biblioteka.delete_book'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(BookDeleteView, self).delete(request, *args, **kwargs)


class AuthorDetailView(generic.DetailView):
    model = Author


class AuthorListView(generic.ListView):
    model = Author


@login_required
@permission_required('biblioteka.add_author')
def author_create_view(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AuthorModelForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            saved = form.save()
            # add a success message
            messages.add_message(request, messages.SUCCESS, _('Author added.'))
            # redirect to a new URL:
            return redirect(reverse_lazy('biblioteka:authorDetail', kwargs={'pk': saved.pk}))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AuthorModelForm()
    return render(request, 'biblioteka/author_create_form.html', context={'form': form})


@login_required
@permission_required('biblioteka.change_author')
def author_update_view(request, pk):
    instance = get_object_or_404(Author, pk=pk)

    if request.method == 'POST':
        form = AuthorModelForm(request.POST, instance=instance)
        if form.is_valid():
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Author updated.'))
            return redirect(reverse_lazy('biblioteka:authorDetail', kwargs={'pk': saved.pk}))

    else:
        form = AuthorModelForm(initial=model_to_dict(instance), auto_id=False)
    return render(request, 'biblioteka/author_update_form.html', context={'form': form})


class AuthorDeleteView(LoginRequiredMixin, SuccessMessageMixin, PermissionRequiredMixin, generic.DeleteView):
    model = Author
    success_url = reverse_lazy('biblioteka:authorList')
    success_message = _('Author deleted.')
    permission_required = 'biblioteka.delete_author'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(AuthorDeleteView, self).delete(request, *args, **kwargs)


class TagDetailView(generic.DetailView):
    model = Tag


class TagListView(generic.ListView):
    model = Tag


@login_required
@permission_required('biblioteka.add_tag')
def tag_create_view(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TagModelForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Tag added.'))
            # redirect to a new URL:
            return redirect(reverse_lazy('biblioteka:tagDetail', kwargs={'pk': saved.pk}))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = TagModelForm()
    return render(request, 'biblioteka/tag_create_form.html', context={'form': form})


@login_required
@permission_required('biblioteka.change_tag')
def tag_update_view(request, pk):
    instance = get_object_or_404(Tag, pk=pk)

    if request.method == 'POST':
        form = TagModelForm(request.POST, instance=instance)
        if form.is_valid():
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Tag updated.'))
            return redirect(reverse_lazy('biblioteka:tagDetail', kwargs={'pk': saved.pk}))

    else:
        form = TagModelForm(initial=model_to_dict(instance), auto_id=False)
    return render(request, 'biblioteka/tag_update_form.html', context={'form': form})


class TagDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, generic.DeleteView):
    model = Tag
    success_url = reverse_lazy('biblioteka:tagList')
    success_message = _('Tag deleted.')
    permission_required = 'biblioteka.delete_tag'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(TagDeleteView, self).delete(request, *args, **kwargs)


class PublisherDetailView(generic.DetailView):
    model = Publisher


class PublisherListView(generic.ListView):
    model = Publisher


@login_required
@permission_required('biblioteka.add_publisher')
def publisher_create_view(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PublisherModelForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Publisher added.'))
            # redirect to a new URL:
            return redirect(reverse_lazy('biblioteka:publisherDetail', kwargs={'pk': saved.pk}))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PublisherModelForm()
    return render(request, 'biblioteka/publisher_create_form.html', context={'form': form})


@login_required
@permission_required('biblioteka.change_publisher')
def publisher_update_view(request, pk):
    instance = get_object_or_404(Publisher, pk=pk)

    if request.method == 'POST':
        form = TagModelForm(request.POST, instance=instance)
        if form.is_valid():
            saved = form.save()
            messages.add_message(request, messages.SUCCESS, _('Publisher updated.'))
            return redirect(reverse_lazy('biblioteka:publisherDetail', kwargs={'pk': saved.pk}))

    else:
        form = PublisherModelForm(initial=model_to_dict(instance), auto_id=False)
    return render(request, 'biblioteka/publisher_update_form.html', context={'form': form})


class PublisherDeleteView(LoginRequiredMixin, PermissionRequiredMixin, SuccessMessageMixin, generic.DeleteView):
    model = Publisher
    success_url = reverse_lazy('biblioteka:publisherList')
    success_message = _('Publisher deleted.')
    permission_required = 'biblioteka.delete_publisher'

    def delete(self, request, *args, **kwargs):
        messages.success(request, self.success_message)
        return super(PublisherDeleteView, self).delete(request, *args, **kwargs)


@login_required
@permission_required('biblioteka.rent_book')
def rental_create_view(request, book_id):
    rental = Rental()
    rental.book = get_object_or_404(Book, pk=book_id)
    rental.rented_by = request.user
    rental.rented_date = datetime.now()

    try:
        rental.clean()
        rental.save()
    except ValidationError as e:
        messages.add_message(request, messages.ERROR, e.message)
        return redirect('biblioteka:bookDetail', pk=book_id)

    messages.add_message(request, messages.SUCCESS, _('Book rented.'))
    return redirect(reverse('biblioteka:bookDetail', args=[book_id]))


@login_required
@permission_required('biblioteka.return_book')
def rental_return_view(request, rental_id):
    rental = get_object_or_404(Rental, pk=rental_id)

    try:
        rental.return_book(request.user)
        messages.add_message(request, messages.SUCCESS, _('Book returned.'))
    except ValidationError as e:
        messages.add_message(request, messages.ERROR, e.message)

    return redirect('biblioteka:bookDetail', pk=rental.book.pk)


@login_required
@permission_required('biblioteka.mark_book_read')
def mark_book_read_view(request, book_id):
    if request.method == 'POST':
        try:
            request.user.userprofile.mark_book_read(book_id)
        except ValidationError as e:
            messages.add_message(request, messages.ERROR, e.message)
            return redirect('biblioteka:bookDetail', pk=book_id)

        messages.add_message(request, messages.SUCCESS, _('Book marked as read.'))
        return redirect(reverse('biblioteka:bookDetail', args=[book_id]))
    else:
        raise Http404()


@login_required
@permission_required('biblioteka.mark_book_unread')
def mark_book_unread_view(request, book_id):
    if request.method == 'POST':
        try:
            request.user.userprofile.mark_book_unread(book_id)
        except ValidationError as e:
            messages.add_message(request, messages.ERROR, e.message)
            return redirect('biblioteka:bookDetail', pk=book_id)

        messages.add_message(request, messages.SUCCESS, _('Book marked as unread.'))
        return redirect(reverse('biblioteka:bookDetail', args=[book_id]))
    else:
        raise Http404()
