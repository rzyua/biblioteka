��    p      �  �         p	     q	  
   	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
  
   )
     4
     B
  	   V
     `
     u
     �
     �
     �
     �
     �
     �
     �
  $   �
     "     ;     V  3   u     �     �     �     �     �     �     �  
               	   '     1     @     I     O  
   V     a  	   f     p     x     }     �     �     �     �     �     �     �  	   �     �               )  
   <     G     L     Q     Y     `     m     s     z     �     �  
   �     �     �     �     �     �     �     �               !     (     -     :     C  8   G  4   �     �     �  
   �  	   �     �     �     �     �  
   �            	     
        &  	   2     <     H     M     S  �  X     �               *  	   <     F     c     q     �     �     �     �     �     �     �     �       %      (   F     o     �     �  	   �     �     �     �  0   
  .   ;  1   j  *   �  *   �  *   �  	        '     >     D     Q     b  	   t     ~     �     �     �  
   �     �     �     �     �     �     �     �     �     �                    =     D     W     c     o     �     �     �     �     �  	   �     �     �               $  	   ,     6     =     J     V     e     r     �     �     �     �     �     �     �     �     
          0  B   4  6   w     �  	   �     �     �     �  	   �     �     �     �        
               	   /     9     L     _     d     k     %          p           E      $   .   2       T             4   6          l               `   c   >      +               &       I   i   '      =   -   N      (   D   ^      X   @   	   o                     G   R                  #      h   k       L   M   !       V   H   U   <       P           [      Y      Z                 a      g           S   ;   1   Q   J          )       e   3          F   _          j   n      f   "   8       O   C       /   *   B      9   K   \   
   ,   ?   A   7      m             5   W   b              ]   0   d          :    Add an author Add author Add book Add publisher Add tag Are you sure you want to delete Author added. Author deleted. Author index Author updated. Authors Available copies Book added. Book count Book deleted. Book doesn't exist. Book list Book marked as read. Book marked as unread. Book rented. Book returned. Book updated. Books Books by this author Books by this publisher Books with this tag Can create a rental for someone else Can mark a book as read. Can mark a book as unread. Can rent a book for themselves Can return a book that was borrowed by someone else Can return a rented book Comment Confirm delete Delete Delete author Delete book Delete publisher Delete tag Edit Edit author Edit book Edit publisher Edit tag Email Empty. First name ISBN Last name Log out Name Nickname No No copies available. None Page %(current)s of %(total)s Password Permission error Publication year Publisher Publisher added. Publisher deleted. Publisher list Publisher updated. Publishers Read Rent Rentals Rented Rented books Reset Return Returned Save Sign in Tag added. Tag deleted. Tag list Tag updated. Tags This ISBN is incorrect. This account is inactive. This is not a valid ISBN. Title Unavailable Unread User User profile Username Yes You can't return a book that was rented by someone else. You don't have the necessary permissions to do this. authors comment first name last name name next nickname no of copies no results of previous publisher read books rented book rented by rented date tags title year Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-07-06 20:04+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: plMIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Dodaj autora Dodaj autora Dodaj książkę Dodaj wydawnictwo Dodaj tag CZy na pewno chcesz usunąć Autor dodany. Autor usunięty. Indeks autorów Autor zaktualizowany. Autorzy Dostępne kopie Książka dodana. Liczba książek Książka usunięta. Książka nie istnieje. Lista książek Książka oznaczona jako przeczytana. Książka oznaczona jako nieprzeczytana. Książka wypożyczona. Książka zwrócona. Książka zaktualizowana. Książki Książki tego autora Książki z tego wydawnictwa Książki z tym tagiem Może wypożyczyć książkę w czyimś imieniu. Może oznaczyć książkę jako przeczytaną. Może oznaczyć książkę jako nieprzeczytaną. Może wypożyczyć książkę dla siebie. Może oddać książkę w czyimś imieniu. Może zwrócić wypożyczoną książkę. Komentarz Potwierdź usunięcie Usuń Usuń autora Usuń książkę Usuń wydawnictwo Usuń tag Edytuj Edytuj autora Edytuj książkę Edytuj wydawnictwo Edytuj tag Email Pusto. Imię ISBN Nazwisko Wyloguj się Nazwa Ksywa Nie Brak dostępnych kopii. Brak Strona %(current)s z %(total)s Hasło Błąd autoryzacji Rok wydania Wydawnictwo Wydawnictwo dodane. Wydawnictwo usunięte. Lista wydawnictw Wydawnictwo zaktualizowane. Wydawnictwa Przeczytana Wypożycz Wypożyczenia Wypożyczona Wypożyczone książki Zresetuj Zwróć Zwrócona Zapisz Zaloguj się Tag dodany. Tag usunięty. Lsita tagów Tag zaktualizowany. Tagi Ten ISBN jest niepoprawny. To konto jest nieaktywne. To nie jest poprawny ISBN. Tytuł Niedostępna Nieprzeczytana Użytkownik Profil użytkownika Nazwa uzytkownika Tak Nie możesz zwrócić książki wypożyczonej przez kogoś innego. Nie masz wystarczających uprawnień żeby to zrobić. autorzy komentarz imię nazwisko nazwa następna ksywa liczba kopii brak wyników z poprzednia wydawnictwo przeczytane książki książka wypożyczona przez data wypożyczenia tagi tytuł rok 