# Biblioteka - Polish translation file.
# This file is distributed under the same license as the Biblioteka package.
# Marcin Żyła veep@openmailbox.org, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-06 20:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: plMIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: biblioteka/admin.py:12 biblioteka/admin.py:13
msgid "User profile"
msgstr "Profil użytkownika"

#: biblioteka/forms.py:49 biblioteka/forms.py:90 biblioteka/forms.py:118
#: biblioteka/forms.py:146
msgid "Save"
msgstr "Zapisz"

#: biblioteka/forms.py:55 biblioteka/forms.py:96 biblioteka/forms.py:124
#: biblioteka/forms.py:152
msgid "Reset"
msgstr "Zresetuj"

#: biblioteka/forms.py:169
msgid "This account is inactive."
msgstr "To konto jest nieaktywne."

#: biblioteka/forms.py:180
msgid "Username"
msgstr "Nazwa uzytkownika"

#: biblioteka/forms.py:181
msgid "Password"
msgstr "Hasło"

#: biblioteka/forms.py:184 biblioteka/forms.py:188
#: biblioteka/templates/biblioteka/partial/navbar.html:60
#: biblioteka/templates/registration/login.html:8
msgid "Sign in"
msgstr "Zaloguj się"

#: biblioteka/models.py:16
msgid "first name"
msgstr "imię"

#: biblioteka/models.py:20
msgid "last name"
msgstr "nazwisko"

#: biblioteka/models.py:48 biblioteka/models.py:62
msgid "name"
msgstr "nazwa"

#: biblioteka/models.py:84
msgid "This is not a valid ISBN."
msgstr "To nie jest poprawny ISBN."

#: biblioteka/models.py:86 biblioteka/templates/biblioteka/book_detail.html:50
msgid "ISBN"
msgstr "ISBN"

#: biblioteka/models.py:91
msgid "title"
msgstr "tytuł"

#: biblioteka/models.py:97
msgid "no of copies"
msgstr "liczba kopii"

#: biblioteka/models.py:103
msgid "authors"
msgstr "autorzy"

#: biblioteka/models.py:109
msgid "tags"
msgstr "tagi"

#: biblioteka/models.py:116
msgid "publisher"
msgstr "wydawnictwo"

#: biblioteka/models.py:123
msgid "year"
msgstr "rok"

#: biblioteka/models.py:130
msgid "comment"
msgstr "komentarz"

#: biblioteka/models.py:155
msgid "Can mark a book as read."
msgstr "Może oznaczyć książkę jako przeczytaną."

#: biblioteka/models.py:156
msgid "Can mark a book as unread."
msgstr "Może oznaczyć książkę jako nieprzeczytaną."

#: biblioteka/models.py:170
msgid "nickname"
msgstr "ksywa"

#: biblioteka/models.py:179
msgid "read books"
msgstr "przeczytane książki"

#: biblioteka/models.py:186 biblioteka/models.py:194
msgid "Book doesn't exist."
msgstr "Książka nie istnieje."

#: biblioteka/models.py:206
msgid "rented book"
msgstr "książka"

#: biblioteka/models.py:212
msgid "rented by"
msgstr "wypożyczona przez"

#: biblioteka/models.py:216
msgid "rented date"
msgstr "data wypożyczenia"

#: biblioteka/models.py:226
msgid "Can rent a book for themselves"
msgstr "Może wypożyczyć książkę dla siebie."

#: biblioteka/models.py:227
msgid "Can return a rented book"
msgstr "Może zwrócić wypożyczoną książkę."

#: biblioteka/models.py:228
msgid "Can create a rental for someone else"
msgstr "Może wypożyczyć książkę w czyimś imieniu."

#: biblioteka/models.py:229
msgid "Can return a book that was borrowed by someone else"
msgstr "Może oddać książkę w czyimś imieniu."

#: biblioteka/models.py:237
msgid "No copies available."
msgstr "Brak dostępnych kopii."

#: biblioteka/models.py:241
msgid "You can't return a book that was rented by someone else."
msgstr "Nie możesz zwrócić książki wypożyczonej przez kogoś innego."

#: biblioteka/tables.py:11
#: biblioteka/templates/biblioteka/partial/author_table.html:62
#: biblioteka/templates/biblioteka/partial/book_table.html:106
#: biblioteka/templates/biblioteka/partial/publisher_table.html:48
#: biblioteka/templates/biblioteka/partial/rental_table.html:36
#: biblioteka/templates/biblioteka/partial/rental_table_for_user.html:38
#: biblioteka/templates/biblioteka/partial/tag_table.html:47
msgid "Empty."
msgstr "Pusto."

#: biblioteka/templates/biblioteka/author_confirm_delete.html:5
msgid "Delete author"
msgstr "Usuń autora"

#: biblioteka/templates/biblioteka/author_confirm_delete.html:13
#: biblioteka/templates/biblioteka/book_confirm_delete.html:13
#: biblioteka/templates/biblioteka/publisher_confirm_delete.html:13
#: biblioteka/templates/biblioteka/tag_confirm_delete.html:13
msgid "Confirm delete"
msgstr "Potwierdź usunięcie"

#: biblioteka/templates/biblioteka/author_confirm_delete.html:21
#: biblioteka/templates/biblioteka/book_confirm_delete.html:24
#: biblioteka/templates/biblioteka/publisher_confirm_delete.html:21
#: biblioteka/templates/biblioteka/tag_confirm_delete.html:21
msgid "Are you sure you want to delete"
msgstr "CZy na pewno chcesz usunąć"

#: biblioteka/templates/biblioteka/author_confirm_delete.html:27
#: biblioteka/templates/biblioteka/book_confirm_delete.html:31
#: biblioteka/templates/biblioteka/publisher_confirm_delete.html:27
#: biblioteka/templates/biblioteka/tag_confirm_delete.html:27
#: lib/python3.5/site-packages/django/forms/widgets.py:571
msgid "Yes"
msgstr "Tak"

#: biblioteka/templates/biblioteka/author_confirm_delete.html:28
#: biblioteka/templates/biblioteka/book_confirm_delete.html:34
#: biblioteka/templates/biblioteka/publisher_confirm_delete.html:28
#: biblioteka/templates/biblioteka/tag_confirm_delete.html:28
#: lib/python3.5/site-packages/django/forms/widgets.py:572
msgid "No"
msgstr "Nie"

#: biblioteka/templates/biblioteka/author_create_form.html:5
#: biblioteka/templates/biblioteka/author_create_form.html:13
msgid "Add an author"
msgstr "Dodaj autora"

#: biblioteka/templates/biblioteka/author_detail.html:17
#: biblioteka/templates/biblioteka/book_create_form.html:5
#: biblioteka/templates/biblioteka/book_create_form.html:13
#: biblioteka/templates/biblioteka/book_list.html:15
msgid "Add book"
msgstr "Dodaj książkę"

#: biblioteka/templates/biblioteka/author_detail.html:22
#: biblioteka/templates/biblioteka/book_detail.html:37
#: biblioteka/templates/biblioteka/publisher_detail.html:19
#: biblioteka/templates/biblioteka/tag_detail.html:19
#: lib/python3.5/site-packages/django/forms/formsets.py:384
msgid "Delete"
msgstr "Usuń"

#: biblioteka/templates/biblioteka/author_detail.html:27
#: biblioteka/templates/biblioteka/book_detail.html:42
#: biblioteka/templates/biblioteka/publisher_detail.html:24
#: biblioteka/templates/biblioteka/tag_detail.html:24
msgid "Edit"
msgstr "Edytuj"

#: biblioteka/templates/biblioteka/author_detail.html:33
msgid "Books by this author"
msgstr "Książki tego autora"

#: biblioteka/templates/biblioteka/author_list.html:5
msgid "Author index"
msgstr "Indeks autorów"

#: biblioteka/templates/biblioteka/author_list.html:13
#: biblioteka/templates/biblioteka/book_detail.html:64
#: biblioteka/templates/biblioteka/partial/book_table.html:12
#: biblioteka/templates/biblioteka/partial/navbar.html:23
msgid "Authors"
msgstr "Autorzy"

#: biblioteka/templates/biblioteka/author_list.html:19
msgid "Add author"
msgstr "Dodaj autora"

#: biblioteka/templates/biblioteka/author_update_form.html:5
#: biblioteka/templates/biblioteka/author_update_form.html:13
msgid "Edit author"
msgstr "Edytuj autora"

#: biblioteka/templates/biblioteka/book_confirm_delete.html:5
msgid "Delete book"
msgstr "Usuń książkę"

#: biblioteka/templates/biblioteka/book_detail.html:19
msgid "Rent"
msgstr "Wypożycz"

#: biblioteka/templates/biblioteka/book_detail.html:25
msgid "Unread"
msgstr "Nieprzeczytana"

#: biblioteka/templates/biblioteka/book_detail.html:31
msgid "Read"
msgstr "Przeczytana"

#: biblioteka/templates/biblioteka/book_detail.html:58
msgid "Available copies"
msgstr "Dostępne kopie"

#: biblioteka/templates/biblioteka/book_detail.html:59
msgid "of"
msgstr "z"

#: biblioteka/templates/biblioteka/book_detail.html:68
#: biblioteka/templates/biblioteka/partial/book_table.html:17
#: biblioteka/templates/biblioteka/partial/navbar.html:28
#: biblioteka/templates/biblioteka/tag_list.html:13
msgid "Tags"
msgstr "Tagi"

#: biblioteka/templates/biblioteka/book_detail.html:74
msgid "Publisher"
msgstr "Wydawnictwo"

#: biblioteka/templates/biblioteka/book_detail.html:82
msgid "Publication year"
msgstr "Rok wydania"

#: biblioteka/templates/biblioteka/book_detail.html:94
msgid "Comment"
msgstr "Komentarz"

#: biblioteka/templates/biblioteka/book_detail.html:104
msgid "Rentals"
msgstr "Wypożyczenia"

#: biblioteka/templates/biblioteka/book_list.html:4
msgid "Book list"
msgstr "Lista książek"

#: biblioteka/templates/biblioteka/book_list.html:10
#: biblioteka/templates/biblioteka/partial/navbar.html:18
msgid "Books"
msgstr "Książki"

#: biblioteka/templates/biblioteka/book_update_form.html:4
#: biblioteka/templates/biblioteka/book_update_form.html:10
msgid "Edit book"
msgstr "Edytuj książkę"

#: biblioteka/templates/biblioteka/partial/author_table.html:7
#: biblioteka/templates/registration/user_profile.html:24
msgid "First name"
msgstr "Imię"

#: biblioteka/templates/biblioteka/partial/author_table.html:12
#: biblioteka/templates/registration/user_profile.html:28
msgid "Last name"
msgstr "Nazwisko"

#: biblioteka/templates/biblioteka/partial/author_table.html:17
#: biblioteka/templates/biblioteka/partial/publisher_table.html:13
#: biblioteka/templates/biblioteka/partial/tag_table.html:12
msgid "Book count"
msgstr "Liczba książek"

#: biblioteka/templates/biblioteka/partial/book_table.html:7
#: biblioteka/templates/biblioteka/partial/rental_table_for_user.html:6
msgid "Title"
msgstr "Tytuł"

#: biblioteka/templates/biblioteka/partial/book_table.html:37
msgid "Unavailable"
msgstr "Niedostępna"

#: biblioteka/templates/biblioteka/partial/book_table.html:65
#: biblioteka/templates/biblioteka/partial/book_table.html:86
msgid "None"
msgstr "Brak"

#: biblioteka/templates/biblioteka/partial/navbar.html:33
#: biblioteka/templates/biblioteka/publisher_list.html:12
msgid "Publishers"
msgstr "Wydawnictwa"

#: biblioteka/templates/biblioteka/partial/navbar.html:43
#, fuzzy
#| msgid "ISBN"
msgid "ISBN-13"
msgstr "ISBN"

#: biblioteka/templates/biblioteka/partial/navbar.html:54
msgid "Log out"
msgstr "Wyloguj się"

#: biblioteka/templates/biblioteka/partial/publisher_table.html:8
#: biblioteka/templates/biblioteka/partial/tag_table.html:7
msgid "Name"
msgstr "Nazwa"

#: biblioteka/templates/biblioteka/partial/rental_table.html:6
msgid "User"
msgstr "Użytkownik"

#: biblioteka/templates/biblioteka/partial/rental_table.html:7
#: biblioteka/templates/biblioteka/partial/rental_table_for_user.html:7
msgid "Rented"
msgstr "Wypożyczona"

#: biblioteka/templates/biblioteka/partial/rental_table.html:8
#: biblioteka/templates/biblioteka/partial/rental_table_for_user.html:8
msgid "Returned"
msgstr "Zwrócona"

#: biblioteka/templates/biblioteka/partial/rental_table.html:26
#: biblioteka/templates/biblioteka/partial/rental_table_for_user.html:28
msgid "Return"
msgstr "Zwróć"

#: biblioteka/templates/biblioteka/publisher_confirm_delete.html:5
msgid "Delete publisher"
msgstr "Usuń wydawnictwo"

#: biblioteka/templates/biblioteka/publisher_create_form.html:5
#: biblioteka/templates/biblioteka/publisher_create_form.html:13
#: biblioteka/templates/biblioteka/publisher_list.html:17
msgid "Add publisher"
msgstr "Dodaj wydawnictwo"

#: biblioteka/templates/biblioteka/publisher_detail.html:30
msgid "Books by this publisher"
msgstr "Książki z tego wydawnictwa"

#: biblioteka/templates/biblioteka/publisher_list.html:5
msgid "Publisher list"
msgstr "Lista wydawnictw"

#: biblioteka/templates/biblioteka/publisher_update_form.html:4
#: biblioteka/templates/biblioteka/publisher_update_form.html:10
msgid "Edit publisher"
msgstr "Edytuj wydawnictwo"

#: biblioteka/templates/biblioteka/tag_confirm_delete.html:5
msgid "Delete tag"
msgstr "Usuń tag"

#: biblioteka/templates/biblioteka/tag_create_form.html:5
#: biblioteka/templates/biblioteka/tag_create_form.html:13
#: biblioteka/templates/biblioteka/tag_list.html:19
msgid "Add tag"
msgstr "Dodaj tag"

#: biblioteka/templates/biblioteka/tag_detail.html:30
msgid "Books with this tag"
msgstr "Książki z tym tagiem"

#: biblioteka/templates/biblioteka/tag_list.html:5
msgid "Tag list"
msgstr "Lsita tagów"

#: biblioteka/templates/biblioteka/tag_update_form.html:5
#: biblioteka/templates/biblioteka/tag_update_form.html:13
msgid "Edit tag"
msgstr "Edytuj tag"

#: biblioteka/templates/django_tables2/bootstrap.html:35
msgid "no results"
msgstr "brak wyników"

#: biblioteka/templates/django_tables2/bootstrap.html:63
msgid "previous"
msgstr "poprzednia"

#: biblioteka/templates/django_tables2/bootstrap.html:67
#, python-format
msgid "Page %(current)s of %(total)s"
msgstr "Strona %(current)s z %(total)s"

#: biblioteka/templates/django_tables2/bootstrap.html:72
msgid "next"
msgstr "następna"

#: biblioteka/templates/registration/login.html:10
#: biblioteka/templates/registration/login.html:30
msgid "Permission error"
msgstr "Błąd autoryzacji"

#: biblioteka/templates/registration/login.html:34
msgid "You don't have the necessary permissions to do this."
msgstr "Nie masz wystarczających uprawnień żeby to zrobić."

#: biblioteka/templates/registration/user_profile.html:34
msgid "Email"
msgstr "Email"

#: biblioteka/templates/registration/user_profile.html:39
msgid "Nickname"
msgstr "Ksywa"

#: biblioteka/templates/registration/user_profile.html:49
msgid "Rented books"
msgstr "Wypożyczone książki"

#: biblioteka/views.py:58
msgid "This ISBN is incorrect."
msgstr "Ten ISBN jest niepoprawny."

#: biblioteka/views.py:104
msgid "Book added."
msgstr "Książka dodana."

#: biblioteka/views.py:123
msgid "Book updated."
msgstr "Książka zaktualizowana."

#: biblioteka/views.py:142
msgid "Book deleted."
msgstr "Książka usunięta."

#: biblioteka/views.py:169
msgid "Author added."
msgstr "Autor dodany."

#: biblioteka/views.py:188
msgid "Author updated."
msgstr "Autor zaktualizowany."

#: biblioteka/views.py:199
msgid "Author deleted."
msgstr "Autor usunięty."

#: biblioteka/views.py:225
msgid "Tag added."
msgstr "Tag dodany."

#: biblioteka/views.py:244
msgid "Tag updated."
msgstr "Tag zaktualizowany."

#: biblioteka/views.py:255
msgid "Tag deleted."
msgstr "Tag usunięty."

#: biblioteka/views.py:281
msgid "Publisher added."
msgstr "Wydawnictwo dodane."

#: biblioteka/views.py:300
msgid "Publisher updated."
msgstr "Wydawnictwo zaktualizowane."

#: biblioteka/views.py:311
msgid "Publisher deleted."
msgstr "Wydawnictwo usunięte."

#: biblioteka/views.py:334
msgid "Book rented."
msgstr "Książka wypożyczona."

#: biblioteka/views.py:345
msgid "Book returned."
msgstr "Książka zwrócona."

#: biblioteka/views.py:362
msgid "Book marked as read."
msgstr "Książka oznaczona jako przeczytana."

#: biblioteka/views.py:378
msgid "Book marked as unread."
msgstr "Książka oznaczona jako nieprzeczytana."