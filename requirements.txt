django >= 2.0.2
django_crispy_forms >= 1.7.0
django_tables2 >= 1.19.0
django_bootstrap_breadcrumbs >= 0.9.0
isbntools >= 4.3.13